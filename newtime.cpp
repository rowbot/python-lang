#include <iostream>
#include <chrono>
#include <ctime>
 
int main()
{
	std::chrono::time_point<std::chrono::system_clock> start,tmp, end;
	start = tmp =  std::chrono::system_clock::now();
	for (int i = 0; i < 100; ++i) {
		auto t = std::chrono::system_clock::now();
		tmp += std::chrono::system_clock::now() - t;
	}
	end = std::chrono::system_clock::now();
	long int nwtime =  std::chrono::duration_cast<std::chrono::nanoseconds>(tmp-start).count();
	int elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	std::cout <<" Time 1: " << nwtime << std::endl << "Time 2: " << elapsed_seconds << "s\n";
}

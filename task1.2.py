#!/usr/bin/python

def similarity (file1, file2):
    dict1 = freq_letters(file1)
    dict2 = freq_letters(file2)
    keys = sorted(set(dict1).union(set(dict2)))  # объединение обоих списков и их сортировка
    xs = [dict1.get (k, 0) for k in keys]  # создание списка колличества встречающихся элементов общего списка в первом словаре
    ys = [dict2.get (k, 0) for k in keys]  
    return rxy (xs, ys)	# возвращает коэффициент корреляции

def freq_letters(filename):  # функция считывания из файла строки, сортировка по ключу (букве), в итоге имеем список, где находятся все буквы, которые были в строке и их кол-во, для совпадения кодов символов мы все переводим в верхний регистр.
    with open (filename, encoding = "UTF-8") as fin:
        letters = {}
        text = fin.read().upper()
        for let in text:
            if let.isalpha():
                letters[let] = letters.get(let, 0)+ 1  # т.е. в начале все нули. Новый символ = новый ключ. Если новый символ, то его значение автоматически в списке ноль, поэтому, чтоб показать, что он нам встретился, мы добавляем единичку.  Так как поиск по ключу то в итоге посчитается кол-во каждого символа в строке. 
        return letters

from math import*
def mean (xs):  # вычисляет коэффициент "встречаемости" что-ли. Делится кол-во всех букв в одной строке на алфавит двух строк
    s = 0
    for x in xs:
        s+=x
    return s/len(xs)

def rxy(xs, ys):  # UPD: прочитал README, это коэффициент корреляции
    xmean = mean (xs)
    ymean = mean (ys)
    s0 = 0
    s1 = 0
    s2 = 0
    for i in range (len(xs)):  
        s0+=(xs[i]- xmean) * (ys[i]-ymean)
        s1+=(xs[i]- xmean)**2
        s2+=(ys[i]- ymean)**2
    return s0/sqrt(s1*s2)

import operator
def find_lang (unk):
    langs = ["be", "be-tarask", "de", "el", "en", "eo", "es", "fr", "la","pt", "ru", "uk"]
    listlist = {}
    for lang in langs:
        listlist[lang + ".txt"] = similarity (unk, "data/"+lang+".txt")
    #sorted_list = sorted(listlist.items(), key = operator.itemgetter(0))
    sorted_list = sorted(listlist.items(), key = lambda x: x[1])
    sorted_list.reverse()
    #print(listlist)
    print(sorted_list)
    #for lang in langs:
    #    m = -1;
    #    for lang2 in langs:
    #        if m < listlist[lang2]:
    #            m = listlist[lang2]
    #    print(m, end=" ")
    #    print("%10.3f" %sorted_list[lang + ".txt"], end = "")

for i in range (12):
    unk = "tests/unknown" + str (i+1)+".txt"
    print(unk)
    find_lang(unk)


#!/usr/bin/python

def similarity (file1, file2):
    dict1 = freq_letters(file1)
    dict2 = freq_letters(file2)
    keys = sorted(set(dict1).union(set(dict2)))  # объединение обоих списков и их сортировка
    xs = [dict1.get (k, 0) for k in keys]  # создание списка колличества встречающихся элементов общего списка в первом словаре
    ys = [dict2.get (k, 0) for k in keys]  
    return rxy (xs, ys)	# возвращает коэффициент корреляции

def freq_letters(filename):  # функция считывания из файла строки, сортировка по ключу (букве), в итоге имеем список, где находятся все буквы, которые были в строке и их кол-во, для совпадения кодов символов мы все переводим в верхний регистр.
    with open (filename, encoding = "UTF-8") as fin:
        letters = {}
        text = fin.read().upper()
        for let in text:
            if let.isalpha():
                letters[let] = letters.get(let, 0)+ 1  # т.е. в начале все нули. Новый символ = новый ключ. Если новый символ, то его значение автоматически в списке ноль, поэтому, чтоб показать, что он нам встретился, мы добавляем единичку.  Так как поиск по ключу то в итоге посчитается кол-во каждого символа в строке. 
        return letters

from math import*
def mean (xs):  # вычисляет коэффициент "встречаемости" что-ли. Делится кол-во всех букв в одной строке на алфавит двух строк
    s = 0
    for x in xs:
        s+=x
    return s/len(xs)

def rxy(xs, ys):  # UPD: прочитал README, это коэффициент корреляции
    xmean = mean (xs)
    ymean = mean (ys)
#print (xmean, ymean)
    s0 = 0
    s1 = 0
    s2 = 0
    for i in range (len(xs)):  # по идее вам должны были этот алгоритм пояснить.. Сейчас поробую загуглить на тему, есть ли другие, более оптимальные
        s0+=(xs[i]- xmean) * (ys[i]-ymean)
        s1+=(xs[i]- xmean)**2
        s2+=(ys[i]- ymean)**2
    return s0/sqrt(s1*s2)

def find_lang (lang):
    smax= -1
    lang_max=""
    for i in range (12):  # самая изи оптимизация, которая прям напрашивается - вычеркивать уже определенные тексты. Можно создать какой-нибудь список уже определенных номеров текстов и пробегаться по этому массиву. Если такой текст уже определен, то continue (начинаем новую итерацию)
        unk = "tests/unknown" + str (i+1)+".txt"
        s = similarity (unk, "data/" + lang+".txt")
        if s> smax:
            smax=s
            lang_max=unk
    print (lang, lang_max, smax)

import time
my_time = time.time()
tmp = time.clock()  # сейчас будет оочень большой комментарий
                    # The method clock() returns the current processor time
                    # as a floating point number expressed in seconds on Unix. 
                    # The precision depends on that of the C function of the same name,
                    # but in any case, this is the function to use for benchmarking Python or timing algorithms.

                    # Тебе надо вот это скорее всего: On Windows, this function returns wall-clock seconds elapsed since
                    # the first call to this function, as a floating point number,
                    # based on the Win32 function QueryPerformanceCounter.
langs = ["be", "be-tarask", "de", "el", "en", "eo", "es", "fr", "la","pt", "ru", "uk"]
for lang in langs:  # типа основной текст программы, пробегаемся по всем примерам языков, где для каждого примера пробегаемся по всем неизвестным спискам (см комммент выше)
    find_lang(lang)
current_time = time.time()
new_tmp = time.clock()
print ("Process time: %f" %new_tmp)
print("Wall time: %f" %(current_time - my_time))


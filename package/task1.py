#!/usr/bin/python

def similarity (file1, file2):
    dict1 = freq_letters(file1)
    dict2 = freq_letters(file2)
    keys = sorted(set(dict1).union(set(dict2)))  # объединение обоих списков и их сортировка
    xs = [dict1.get (k, 0) for k in keys]  # создание списка колличества встречающихся элементов общего списка в первом словаре
    ys = [dict2.get (k, 0) for k in keys]  
    return rxy (xs, ys)	# возвращает коэффициент корреляции

def freq_letters(filename):  # функция считывания из файла строки, сортировка по ключу (букве), в итоге имеем список, где находятся все буквы, которые были в строке и их кол-во, для совпадения кодов символов мы все переводим в верхний регистр.
    with open (filename, encoding = "UTF-8") as fin:
        letters = {}
        text = fin.read().upper()
        for let in text:
            if let.isalpha():
                letters[let] = letters.get(let, 0)+ 1  # т.е. в начале все нули. Новый символ = новый ключ. Если новый символ, то его значение автоматически в списке ноль, поэтому, чтоб показать, что он нам встретился, мы добавляем единичку.  Так как поиск по ключу то в итоге посчитается кол-во каждого символа в строке. 
        return letters

from math import*
def mean (xs):  # вычисляет коэффициент "встречаемости" что-ли. Делится кол-во всех букв в одной строке на алфавит двух строк
    s = 0
    for x in xs:
        s+=x
    return s/len(xs)

def rxy(xs, ys):  # UPD: прочитал README, это коэффициент корреляции
    xmean = mean (xs)
    ymean = mean (ys)
#print (xmean, ymean)
    s0 = 0
    s1 = 0
    s2 = 0
    for i in range (len(xs)):  # по идее вам должны были этот алгоритм пояснить.. Сейчас поробую загуглить на тему, есть ли другие, более оптимальные
        s0+=(xs[i]- xmean) * (ys[i]-ymean)
        s1+=(xs[i]- xmean)**2
        s2+=(ys[i]- ymean)**2
    return s0/sqrt(s1*s2)

def find_lang (langs):
    for lang in langs:
        print ("\n %10s" %lang, end = " ")
        for lang2 in langs:  
            lan = lang2 + ".txt"
            s = similarity (lan, lang+".txt")  
            print ("%10.3f" % s, end = " ")
    

langs = ["be", "be-tarask", "de", "el", "en", "eo", "es", "fr", "la","pt", "ru", "uk"]
print("%10s" %"", end = "")
for lang in langs:
    print("%10s" %lang, end = " ")
find_lang(langs)

